#!/bin/bash

for i in `seq 1 5`; do
    if [ ! -e enron$i.tar.gz ]; then
        wget http://nlp.cs.aueb.gr/software_and_datasets/Enron-Spam/preprocessed/enron$i.tar.gz
    fi   
    tar xfz enron$i.tar.gz
    for d in spam ham; do mkdir -p $d; mv enron$i/$d/* $d/; done
    rm -rf enron$i
    #rm enron$i.tar.gz
done
