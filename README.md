
# Bayesian spam classifier

Written as an excercise in ~100 lines of JS with no dependencies.

## Inspiration

https://github.com/tsoding/vetcheena

## Quickstart

   ```console
   $ yarn run enron-test
   ```

This will download the enron spam dataset to `data/` and run the program.
