const fs = require('fs');
const path = require('path');

// Spam if spamicity is above, ham if below
const SPAM_THRESHOLD = 0.56;
const MIN_WORD_LEN = 4;
// Change to allow more characters in words (retain whitespace chars, or word-split fails)
const validCharsRe = /[^åäöa-zA-Z\ \t\n\r]/g;

(async function() {
    const text_to_words = text => text.replace(validCharsRe, '')
	  .split(/[\ \t\r\n]/mg)
	  .filter(x => x !== '')
	  .map(word => word.toLowerCase());

    const text_to_bow = (text) => {
	const words = Object.create(null);
	text_to_words(text)
	    .filter(word => word.length >= MIN_WORD_LEN)
	    .map(word => typeof words[word] != 'undefined' ? words[word]++ : words[word]=1); 
	return words;
    };

    const read_file_as_bow = (file) => {
	return text_to_bow(fs.readFileSync(file).toString('utf-8'));
    };

    const process_dir = (dir) => {
	return new Promise((resolve, reject) => {
	    fs.readdir(dir, (err, files) => err ? reject('ERROR: Could not read dir ' + dir + ':' + err) : 
		       resolve(join_bows(files.map(file => read_file_as_bow(path.join(dir, file)))))
		      );
	});
    };

    const join_bows = bows => {
	const newBow = Object.create(null);
	for(let bi in bows) {
	    const bow = bows[bi];
	    for(let word in bow) {
		newBow[word] = typeof newBow[word] == 'undefined' ? bow[word] : bow[word] + newBow[word];
	    }
	}

	const sum = Object.keys(newBow).reduce((a,v) => a + newBow[v], 0);
	Object.keys(newBow).forEach(word => newBow[word] = newBow[word] / sum);
	return newBow;
    }

    const wordProbabilitySpam = word => {
	const ham = ham_bow[word] || false;
	const spam = spam_bow[word] || false;
	if(ham === false && spam === false) {
	    return NaN; // not seen
	}
	const p1 = ham === false ? 1-spam : ham;
	const p2 = spam === false ? 1-ham : spam;
	// pws / (pws + phs)
	return p1 / (p1 + p2);
    };

    const textProbabilitySpam = text => {
	const pbs = text_to_words(text)
	      .map(word => wordProbabilitySpam(word))
	      .filter(x => !Number.isNaN(x));
	const ipbs = pbs.map(x => 1-x);
	const pp = pbs.reduce((a,v) => a*v, 1);
	const ipp = ipbs.reduce((a,v) => a*v, 1);
	const r = (pp / (pp + ipp));
	return isNaN(r) ? 0 : r;
    };

    const ham_path = process.argv[2] || 'data/ham';
    const spam_path = process.argv[3] || 'data/spam';

    console.log('Building statistical model..');
    const t1 = Date.now();
    const ham_bow = await process_dir(ham_path, 'ham');
    const spam_bow = await process_dir(spam_path, 'spam');
    console.log('Done, took %dms..', Date.now()-t1);

    const validate = dir => new Promise((resolve, reject) => {
	fs.readdir(dir, (err, files) => {
	    if(err) {
		return reject(err);
	    }
	    resolve(files.map(file => {
		const filep = path.join(dir, file)
		const text = fs.readFileSync(filep).toString('utf-8');
		const prob_spam = textProbabilitySpam(text);
		//console.log('File: %s: %d', filep, Math.floor(prob*100));
		return [file, prob_spam];
	    }));
	    
	});
    });

    console.log('Running classification..');
    const t2 = Date.now();
    const spam_validation = (await validate(spam_path));
    const ham_validation = (await validate(ham_path));

    const spam_avg = spam_validation.filter(x => x[1] >= SPAM_THRESHOLD).length / spam_validation.length;
    const ham_avg = ham_validation.filter(x => x[1] < SPAM_THRESHOLD).length / ham_validation.length;

    console.log('Done, took %dms..', Date.now()-t2);
    console.log('SPAM validation avg: %s%% correct for %d samples', Math.floor(ham_avg*1000)/10, spam_validation.length);
    console.log('HAM validation avg: %s%% correct for %d samples', Math.floor(spam_avg*1000)/10, ham_validation.length);
})();
